<?php

namespace joyqhs\Framework\Contract;

interface Arrayable
{
    public function toArray(): array;
}