<?php

namespace joyqhs\Framework\Contract;

interface Jsonable
{
    public function __toString(): string;
}